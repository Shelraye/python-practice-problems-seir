# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if values == []:
        return None
    maxL = 0 
    for val in values:
        if val > maxL:
            maxL = val
    return maxL


print(max_in_list([]))