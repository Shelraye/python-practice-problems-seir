# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# secon largest stored in k, at index 1
# emtpy == none
# sort(0, -1, -1)
#  if len >2 == none 

def find_second_largest(values):
    if len(values) <= 1 or values == []:
        return None
    values.sort()
    k = values[-2]
    return k


print(find_second_largest([3, 4, 9, 12]))